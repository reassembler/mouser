// give IO ports names

int internalLed = 13;
int externalLed = 3;
int space = 400;
int led = externalLed;
int killSwitch = 5;

// the setup routine runs once when you press reset:
void setup() {                
  pinMode(led, OUTPUT);     
  pinMode(externalLed, OUTPUT);
  pinMode(killSwitch, INPUT);
  
  digitalWrite(internalLed, LOW);
  digitalWrite(externalLed, LOW);

  lightOn();

  delay(1000);

  lightOff();

  delay(1000);  
  
}

// the loop routine runs over and over again forever:
void loop() {
  if (mouseIsDead()) {
    lightOn();
  }
  else {
    lightOff();
  }
  
  delay(1000);  
   
 
}

int mouseIsDead() {
  return !digitalRead(killSwitch);
}

void lightOn() {
  digitalWrite(externalLed, HIGH);
}

void lightOff() {
  digitalWrite(externalLed, LOW);
}


void sos() {
  tick();
  tick();
  tick();
  delay(100);
  dash();
  dash();
  dash();
  delay(100);
  tick();
  tick();
  tick();
  
}

void tick() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(200);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(200);
}  


void dash() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(space);
}  
