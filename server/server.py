import socket, datetime, threading, os

class AServer(threading.Thread):
    def __init__(self, port):
        threading.Thread.__init__(self)
        self.port = port

    def now(self):
        d = datetime.datetime.now()
        return d.strftime("%d/%m/%y %H:%M:%S")

    def run(self):
        host = ''
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        s.bind((host, self.port))
        print 'server started successfully, waiting...'
        s.listen(1)
        conn, addr = s.accept()
        print 'contact', addr, 'on', self.now()

        while 1:
            try:
                data = conn.recv(1024)
            except socket.error:
                print 'lost', addr, 'waiting..'
                s.listen(1)
                conn, addr = s.accept()
                print 'contact', addr, 'on', self.now()
                continue

            if not data:
                print 'lost', addr, 'waiting..'
                s.listen(1)
                conn, addr = s.accept()
                print 'contact', addr, 'on', self.now()
            else:    
                print "received msg: '", data, "'"
                
                if data.strip() == 'close':
		    print 'received close message, shutting down'
		    s.close()
		    exit(0)

                conn.send('roger')
                conn.close()



t = AServer(31944)
t.start()
 
 
