int led = 13;
int buzzer = 4;
int motion = 2;


void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);  
  digitalWrite(buzzer, LOW);
  
  pinMode(buzzer, OUTPUT); 
  digitalWrite(buzzer, LOW);

  pinMode(motion, INPUT);
    
}


void loop() {
  
  if (digitalRead(motion) == HIGH) {
    buzzFlash();      
  }
  
  
  delay(2000);               // wait for a second
}


void buzzFlash() {
  digitalWrite(buzzer, HIGH);   // turn the LED on (HIGH is the voltage level)      
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)

  delay(1000);               // wait for a second

  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(buzzer, LOW);    // turn the LED off by making the voltage LOW

}
